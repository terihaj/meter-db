-- postgresql
drop trigger if exists "meter_reading_new" on "meter_reading";
drop function if exists "meter_active_update";
drop table if exists "meter_access";
drop table if exists "meter_reading";
drop table if exists "meter";
drop table if exists "user";

create table "user" (
	"id" serial primary key,
	"name" varchar unique not null,
	"password" varchar not null
);

create table "meter" (
	"id" serial primary key,
	"name" varchar unique not null,
	"key" varchar unique not null,
	"last_active_at" timestamptz
);

create table "meter_reading" (
	"id" serial primary key,
	"meter_id" int references "meter"(id) on delete cascade,
	"value" int not null check (value >= 0),
	"ended_at" timestamptz not null,
	"duration" int not null check (duration >= 0)
);

create table "meter_access" (
	"user_id" int references "user"(id) on delete cascade,
	"meter_id" int references "meter"(id) on delete cascade,

    primary key ("user_id", "meter_id")
);

create or replace function meter_active_update() returns trigger
	language plpgsql
	as $$
begin
	if (TG_OP = 'INSERT') then
		update meter
		set last_active_at = NEW.ended_at
		where "id" = NEW.meter_id;
	end if;

	return new;
end;
$$;

create trigger "meter_reading_new"
after insert on "meter_reading"
for each row
execute procedure meter_active_update();